## Prerequisites

For running the testsuite:

$ sudo apt install -y \
      python3-pytest \
      python3-flake8 \
      python3-coverage


## Testing

$ make check

or

$ pytest-3
